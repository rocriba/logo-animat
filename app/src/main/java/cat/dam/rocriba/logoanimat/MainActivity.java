package cat.dam.rocriba.logoanimat;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //For every final declare every image as a constant
        final ImageView iv_logo = (ImageView) findViewById(R.id.iv_logo);
        //For every image clicked, start the animation
        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animacioPilota = AnimationUtils.loadAnimation(MainActivity.this, R.anim.botar);
                iv_logo.startAnimation(animacioPilota);
            }
        });
        final ImageView iv_lleo = (ImageView) findViewById(R.id.iv_lleo);
        iv_lleo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.apareixer);
                iv_lleo.startAnimation(animation);
            }
        });
        final ImageView iv_elefant = (ImageView) findViewById(R.id.iv_elefant);
        iv_elefant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rodar);
                iv_elefant.startAnimation(animation);
            }
        });

        final ImageView iv_rodar = (ImageView) findViewById(R.id.iv_rodar);
        iv_rodar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.venir);
                iv_rodar.startAnimation(animation);
            }
        });

    }
}